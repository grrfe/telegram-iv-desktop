# iv-desktop

(Work-in-progress) [Telegram Instant View](https://instantview.telegram.org/) "client" built on top of [TDLib](https://github.com/tdlib/td). Allows the user to view instant view pages directly in their browser by requesting a given page from Telegram via TDLib, and then generating HTML from the response. 

![](readme_assets/screen1.png)

## Setup

* Clone project `https://gitlab.com/grrfe/iv-desktop`
* [Create a Telegram application](https://core.telegram.org/api/obtaining_api_id)
* [Build TDLib for your platform](https://github.com/tdlib/td/tree/master/example/java)
* Add built TDLib path (`.dll`/`.so`) (`tdlibPath`), `apiId` and `apiHash` to `example.config.json`
* Launch via `./gradlew run --args='example.config.json'`
    * (First run) Follow instructions (login via phone number)
* Webserver starts on `localhost:7000`    

\[Repo-Icon attribution: Icon made by fjstudio from [flaticon.com](https://flaticon.com)\]
