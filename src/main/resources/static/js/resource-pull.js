const WS_CON_STR = location.protocol === "https:" ? "wss://" : "ws://";


function getMeta(property) {
    return document.querySelector("meta[property='" + property + "']").getAttribute("content");
}

document.addEventListener("DOMContentLoaded", () => {
    let originalUrl = getMeta("originalUrl");
    console.log("Viewing IV page of " + originalUrl);

    let ivPageId = getMeta("ivPageId");


    let timestamps = document.getElementsByClassName("timestamp");
    for (let i = 0; i < timestamps.length; i++) {
        let timestamp = timestamps.item(i);
        timestamp.innerText = new Date(parseInt(timestamp.innerText)).toDateString();
    }

    let resourcePullItems = document.getElementsByClassName("wsPull");
    const resItemDict = {};
    if (resourcePullItems.length > 0) {
        const socket = new WebSocket(WS_CON_STR + location.host + "/ws/res");
        socket.addEventListener("open", (event) => {
            for (let i = 0; i < resourcePullItems.length; i++) {
                let item = resourcePullItems.item(i);
                let resId = item.getAttribute("resId");

                resItemDict[resId] = item;
                console.log("Requesting " + resId + " from backend");
                socket.send(JSON.stringify({
                    ivPageId: ivPageId,
                    resId: resId
                }));
            }
        });


        socket.addEventListener("message", (event) => {
            console.log("Received " + event.data);
            let json = JSON.parse(event.data);
            let resId = json["resId"];

            let item = resItemDict[resId];
            item.innerHTML = json["component"];

            delete resItemDict[resId];
            if (Object.keys(resItemDict).length === 0) {
                console.log("No more resources to fetch, closing socket.");
                socket.close();
            }
        });
    }
});
