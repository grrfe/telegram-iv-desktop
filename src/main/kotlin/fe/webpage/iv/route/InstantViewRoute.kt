package fe.webpage.iv.route

import fe.webpage.iv.buildInstantViewPage
import io.javalin.http.Context


object InstantViewRoute {
    private const val HTML_CONTENT_TYPE = "text/html; charset=utf-8"
    private const val CONTENT_TYPE_HEADER = "Content-Type"

    fun formPage(ctx: Context) {
        ctx.formParam("url")?.let { url ->
            sendPage(ctx, url)
        } ?: ctx.result("No url provided!")
    }

    data class ApiRequest(val url: String)

    fun api(ctx: Context) {
        ctx.bodyValidator<ApiRequest>().getOrNull()?.let {
            sendPage(ctx, it.url)
        } ?: ctx.result("Invalid body data!")
    }

    private fun sendPage(ctx: Context, url: String) {
//        val sent = if (CONFIG.cacheBuiltPages) {
//            val file = File(PAGE_CACHE_DIR, Base64.getEncoder().encodeToString(url.toByteArray()) + ".html")
//            if (file.exists()) {
//                ctx.header(CONTENT_TYPE_HEADER, HTML_CONTENT_TYPE).result(FileInputStream(file))
//                true
//            } else false
//        } else false
//
//        if (!sent) {
            ctx.header(CONTENT_TYPE_HEADER, HTML_CONTENT_TYPE).result(buildInstantViewPage(ctx, url))
//        }
    }
}