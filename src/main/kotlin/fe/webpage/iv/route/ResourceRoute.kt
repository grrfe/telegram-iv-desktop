package fe.webpage.iv.route

import com.google.gson.JsonObject
import fe.gson.extensions.parseStringAs
import fe.webpage.iv.findPage
import fe.webpage.iv.getAsStringIfHas
import io.javalin.websocket.WsMessageContext
import io.javalin.websocket.WsMessageHandler

object ResourceRoute : WsMessageHandler {
    override fun handleMessage(ctx: WsMessageContext) {
        val obj = parseStringAs<JsonObject>(ctx.message())
        val resId = obj.getAsStringIfHas("resId")
        val ivPageId = obj.getAsStringIfHas("ivPageId")

        if (resId != null && ivPageId != null) {
            findPage(ivPageId)?.requestAsyncComponent(resId, ctx)
        }
    }
}