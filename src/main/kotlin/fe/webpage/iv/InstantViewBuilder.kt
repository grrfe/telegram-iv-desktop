package fe.webpage.iv

import fe.webpage.iv.html.InstantViewPage
import io.javalin.http.Context
import org.drinkless.tdlib.Connector
import org.drinkless.tdlib.TdApi
import java.net.HttpURLConnection
import java.util.concurrent.CompletableFuture

private val IV_PAGES = mutableSetOf<InstantViewPage>()

fun findPage(ivPageId: String) = IV_PAGES.find { it.pageId == ivPageId }

fun buildInstantViewPage(context: Context, url: String): CompletableFuture<String> {
    val ivPage = InstantViewPage(url).also {
        IV_PAGES.add(it)
    }

    return future<Pair<Int, String>> {
        Connector.getClient().send(TdApi.GetWebPageInstantView(url, true), { obj ->
            if (obj is TdApi.Error && obj.code == 404) {
                this.complete(HttpURLConnection.HTTP_NOT_FOUND to "Unsupported URL!")
            } else this.completeAsync {
                HttpURLConnection.HTTP_OK to ivPage.html(obj as TdApi.WebPageInstantView)
            }
        }, {
            this.complete(HttpURLConnection.HTTP_INTERNAL_ERROR to "Server error!")
        })
    }.thenApply {
        context.status(it.first)
        it.second
    }
}

fun <T> future(future: CompletableFuture<T>.() -> Unit) = CompletableFuture<T>().apply(future)