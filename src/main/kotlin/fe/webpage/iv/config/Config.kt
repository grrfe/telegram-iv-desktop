package fe.webpage.iv.config

import org.drinkless.tdlib.TdApi

data class Config(
    val tdlibPath: String,
    val cacheBuiltPages: Boolean,
    val parameters: TdApi.TdlibParameters
)