package fe.webpage.iv.html

import io.javalin.websocket.WsMessageContext
import j2html.TagCreator
import j2html.attributes.Attr
import org.drinkless.tdlib.Client
import org.drinkless.tdlib.Connector
import org.drinkless.tdlib.TdApi
import java.util.*

class InstantViewPage(val url: String, val pageId: String = UUID.randomUUID().toString()) {
    private val cacheName = Base64.getEncoder().encodeToString(url.toByteArray()) + ".html"

    val requestedResources = mutableMapOf<String, WsMessageContext>()
    val cachedComponents = mutableMapOf<String, String>()

    fun requestAsyncComponent(resId: String, ctx: WsMessageContext) {
        cachedComponents[resId]?.let { component ->
            pushComponent(ctx, resId, component)
            cachedComponents.remove(resId)
        } ?: requestedResources.put(resId, ctx)
    }

    fun pushComponent(context: WsMessageContext, resourceId: String, component: String) {
        context.send(mapOf("resId" to resourceId, "component" to component))
    }

    fun <T : TdApi.Object> registerAsyncDownload(
        file: TdApi.File,
        pageBlock: T,
        resource: DownloadableResource<T>
    ): String {
        val resId = UUID.randomUUID().toString()
        downloadFile(file, { obj ->
            val localFile = (obj as TdApi.File).local
            if (localFile != null && localFile.path.isNotEmpty()) {

                val component = resource.createComponent(this, pageBlock, resource.findSrc(localFile)).render()

                this.requestedResources[resId]?.let { context ->
                    println("Client requested file has been downloaded, pushing to client..")
                    this.pushComponent(context, resId, component)
                    this.requestedResources.remove(resId)
                } ?: this.cachedComponents.put(resId, component).also {
                    println("Download finished before client request, caching..")
                }
            }
        }, { ex -> println(ex) })

        return resId
    }

    fun downloadFile(file: TdApi.File, resultHandler: Client.ResultHandler, errorHandler: Client.ExceptionHandler) {
        Connector.getClient().send(TdApi.DownloadFile(file.id, 32, 0, 0, true), resultHandler, errorHandler)
    }

    fun html(webPage: TdApi.WebPageInstantView): String {
        return TagCreator.html(
            TagCreator.head(
                TagCreator.title("IV - $url"),
                TagCreator.meta().withCharset("utf-8"),
                TagCreator.meta().attr("property", "originalUrl").attr("content", url),
                TagCreator.meta().attr("property", "ivPageId").attr("content", this.pageId),
                TagCreator.link().withRel("stylesheet").withHref("/static/css/normalize.css"),
                TagCreator.link().withRel("stylesheet").withHref("/static/css/default.css"),
                TagCreator.script().withSrc("/static/js/resource-pull.js")
            ).attr(Attr.DIR, if (webPage.isRtl) "rtl" else "ltr"),
            TagCreator.body(
                TagCreator.a("← Original Page").withHref(url).attr(Attr.TARGET, "_blank"),
                TagCreator.each(webPage.pageBlocks.toList()) { block ->
                    findAppendHtml(this, block)
                }
            )
        ).render()
//        .also { page ->
//        if (CONFIG.cacheBuiltPages) {
////            File(PAGE_CACHE_DIR, Base64.getEncoder().encodeToString(url.toByteArray()) + ".html").writeText(page)
////        }
//    }
    }


}
