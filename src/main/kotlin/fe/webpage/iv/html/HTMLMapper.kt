package fe.webpage.iv.html

import fe.webpage.iv.*
import j2html.TagCreator
import j2html.attributes.Attr
import j2html.tags.DomContent
import org.drinkless.tdlib.TdApi

fun <T : TdApi.Object> findHtml(item: T): HtmlMapper<T>? {
    return when (item) {
        is TdApi.RichTextReference -> HtmlRichTextReference
        is TdApi.RichTextSubscript -> HtmlRichTextSubscript
        is TdApi.RichTextSuperscript -> HtmlRichTextSuperscript
        is TdApi.RichTextPhoneNumber -> HtmlRichTextPhoneNumber
        is TdApi.RichTextMarked -> HtmlRichTextMarked
        is TdApi.RichTextIcon -> HtmlRichTextIcon
        is TdApi.RichTextEmailAddress -> HtmlRichTextEmailAddress
        is TdApi.RichTextAnchorLink -> HtmlRichTextAnchorLink
        is TdApi.RichTextAnchor -> HtmlRichTextAnchor
        is TdApi.RichTextUnderline -> HtmlRichTextUnderline
        is TdApi.RichTextStrikethrough -> HtmlRichTextStrikethrough
        is TdApi.RichTextFixed -> HtmlRichTextFixed
        is TdApi.RichTextUrl -> HtmlRichTextUrl
        is TdApi.RichTexts -> HtmlRichTexts
        is TdApi.RichTextPlain -> HtmlRichTextPlain
        is TdApi.RichTextBold -> HtmlRichTextBold
        is TdApi.RichTextItalic -> HtmlRichTextItalic

        is TdApi.PageBlockCaption -> HtmlPageBlockCaption
        is TdApi.PageBlockCover -> HtmlPageBlockCover
        is TdApi.PageBlockEmbedded -> HtmlPageBlockEmbedded
        is TdApi.PageBlockVideo -> HtmlPageBlockVideo
        is TdApi.PageBlockAudio -> HtmlPageBlockAudio
        is TdApi.PageBlockAnimation -> HtmlPageBlockAnimation
        is TdApi.PageBlockAnchor -> HtmlPageBlockAnchor
        is TdApi.PageBlockPreformatted -> HtmlPageBlockPreformatted
        is TdApi.PageBlockPhoto -> HtmlPageBlockPhoto
        is TdApi.PageBlockBlockQuote -> HtmlPageBlockBlockQuote
        is TdApi.PageBlockTitle -> HtmlPageBlockTitle
        is TdApi.PageBlockSubheader -> HtmlPageBlockSubheader
        is TdApi.PageBlockSubtitle -> HtmlPageBlockSubtitle
        is TdApi.PageBlockHeader -> HtmlPageBlockHeader
        is TdApi.PageBlockAuthorDate -> HtmlPageBlockAuthorDate
        is TdApi.PageBlockParagraph -> HtmlPageBlockParagraph
        is TdApi.PageBlockDivider -> HtmlPageBlockDivider
        is TdApi.PageBlockList -> HtmlPageBlockList
        is TdApi.PageBlockListItem -> HtmlPageBlockListItem
        is TdApi.PageBlockPullQuote -> HtmlPageBlockPullQuote
        is TdApi.PageBlockTable -> HtmlPageBlockTable
        is TdApi.PageBlockTableCell -> HtmlPageBlockTableCell
        is TdApi.PageBlockDetails -> HtmlPageBlockDetails
        is TdApi.PageBlockFooter -> HtmlPageBlockFooter
        is TdApi.PageBlockRelatedArticles -> HtmlPageBlockRelatedArticles
        is TdApi.PageBlockRelatedArticle -> HtmlPageBlockRelatedArticle

        else -> null
    } as? HtmlMapper<T>
}

fun <T : TdApi.Object> findStyle(item: T): String? {
    return when (item) {
        is TdApi.PageBlockHorizontalAlignmentCenter -> "text-align: center"
        is TdApi.PageBlockHorizontalAlignmentLeft -> "text-align: left"
        is TdApi.PageBlockHorizontalAlignmentRight -> "text-align: right"
        is TdApi.PageBlockVerticalAlignmentBottom -> "vertical-align: bottom"
        is TdApi.PageBlockVerticalAlignmentTop -> "vertical-align: top"
        is TdApi.PageBlockVerticalAlignmentMiddle -> "vertical-align: middle"
        else -> null
    }
}


fun <T : TdApi.Object> findAppendHtml(page: InstantViewPage, item: T): DomContent? {
    return findHtml(item)?.appendHtml(page, item)
}

object HtmlPageBlockRelatedArticle : HtmlMapper<TdApi.PageBlockRelatedArticle>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockRelatedArticle): DomContent {
        //TODO: implement this
        return TagCreator.span("not implemented yet!")
    }
}

object HtmlPageBlockRelatedArticles : HtmlMapper<TdApi.PageBlockRelatedArticles>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockRelatedArticles): DomContent {
        //TODO: doc says related block which doesnt exist in html spec
        return TagCreator.div(
            findAppendHtml(page, pageBlock.header),
            TagCreator.each(pageBlock.articles.toList()) { block ->
                findAppendHtml(page, block)
            }
        )
    }
}

object HtmlPageBlockFooter : HtmlMapper<TdApi.PageBlockFooter>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockFooter): DomContent {
        return TagCreator.footer(findAppendHtml(page, pageBlock.footer))
    }
}

object HtmlPageBlockDetails : HtmlMapper<TdApi.PageBlockDetails>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockDetails): DomContent {
        return TagCreator.details(
            TagCreator.summary(findAppendHtml(page, pageBlock.header)),
            TagCreator.each(pageBlock.pageBlocks.toList()) { block ->
                findAppendHtml(page, block)
            }
        ).condAttr(pageBlock.isOpen, "open", null)
    }
}

object HtmlPageBlockTableCell : HtmlMapper<TdApi.PageBlockTableCell>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockTableCell): DomContent {
        val content = findAppendHtml(page, pageBlock.text)
        val elem = if (pageBlock.isHeader) TagCreator.th(content) else TagCreator.td(content)

        return elem
            .withStyle(findStyle(pageBlock.align))
            .withStyle(findStyle(pageBlock.valign))
            .condAttr(pageBlock.colspan != 1, "colspan", "${pageBlock.colspan}")
            .condAttr(pageBlock.colspan != 1, "rowspan", "${pageBlock.rowspan}")

    }
}

object HtmlPageBlockTable : HtmlMapper<TdApi.PageBlockTable>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockTable): DomContent {
        return TagCreator.table(
            TagCreator.caption(findAppendHtml(page, pageBlock.caption)),
        )
    }
}

object HtmlPageBlockPullQuote : HtmlMapper<TdApi.PageBlockPullQuote>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockPullQuote): DomContent {
        return TagCreator.aside(findAppendHtml(page, pageBlock.credit), findAppendHtml(page, pageBlock.text))
    }
}

object HtmlPageBlockList : HtmlMapper<TdApi.PageBlockList>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockList): DomContent {
        //TODO: when are we going to use ol?
        return TagCreator.ul(TagCreator.each(pageBlock.items.toList()) { item ->
            findAppendHtml(page, item)
        })
    }
}

object HtmlPageBlockListItem : HtmlMapper<TdApi.PageBlockListItem>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockListItem): DomContent {
        //TODO: decide what to do with caption
        return TagCreator.li(TagCreator.each(pageBlock.pageBlocks.toList()) { block ->
            findAppendHtml(page, block)
        })
    }
}

object HtmlPageBlockAnchor : HtmlMapper<TdApi.PageBlockAnchor>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockAnchor): DomContent {
        //TODO: implement this? doc: * An invisible anchor on a page, which can be used in a URL to open the page from the specified anchor.
        return TagCreator.span("not implemented yet")
    }
}

object HtmlRichTextSubscript : HtmlMapper<TdApi.RichTextSubscript>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextSubscript): DomContent {
        return TagCreator.sub(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextSuperscript : HtmlMapper<TdApi.RichTextSuperscript>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextSuperscript): DomContent {
        return TagCreator.sup(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextReference : HtmlMapper<TdApi.RichTextReference>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextReference): DomContent {
        //TODO: maybe do some specific markup/styling?
        // telegram doc says reference elem which is not in html spec: https://instantview.telegram.org/docs#supported-types
        return TagCreator.a(findAppendHtml(page, pageBlock.text)).withHref(pageBlock.url).withName(pageBlock.anchorName)
    }
}

object HtmlRichTextPhoneNumber : HtmlMapper<TdApi.RichTextPhoneNumber>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextPhoneNumber): DomContent {
        return TagCreator.a(findAppendHtml(page, pageBlock.text)).withHref("tel:${pageBlock.phoneNumber}")
    }
}

object HtmlRichTextMarked : HtmlMapper<TdApi.RichTextMarked>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextMarked): DomContent {
        return TagCreator.mark(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextIcon : HtmlMapper<TdApi.RichTextIcon>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextIcon): DomContent {


        //TODO: check how to best implement this
        return TagCreator.span("not implemented yet!")
    }
}

object HtmlRichTextEmailAddress : HtmlMapper<TdApi.RichTextEmailAddress>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextEmailAddress): DomContent {
        return TagCreator.a(findAppendHtml(page, pageBlock.text)).withHref("mailto:${pageBlock.emailAddress}")
    }
}

object HtmlRichTextAnchorLink : HtmlMapper<TdApi.RichTextAnchorLink>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextAnchorLink): DomContent {
        return TagCreator.a(findAppendHtml(page, pageBlock.text)).withHref(pageBlock.url).withName(pageBlock.anchorName)
    }
}

object HtmlRichTextAnchor : HtmlMapper<TdApi.RichTextAnchor>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextAnchor): DomContent {
        return TagCreator.a().withName(pageBlock.name)
    }
}

object HtmlRichTextUnderline : HtmlMapper<TdApi.RichTextUnderline>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextUnderline): DomContent {
        return TagCreator.ins(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextStrikethrough : HtmlMapper<TdApi.RichTextStrikethrough>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextStrikethrough): DomContent {
        return TagCreator.del(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextFixed : HtmlMapper<TdApi.RichTextFixed>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextFixed): DomContent {
        return TagCreator.code(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlPageBlockDivider : HtmlMapper<TdApi.PageBlockDivider>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockDivider): DomContent {
        return TagCreator.hr(TagCreator.attrs(".divider"))
    }
}

object HtmlPageBlockSubtitle : HtmlMapper<TdApi.PageBlockSubtitle>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockSubtitle): DomContent {
        return TagCreator.h3(
            findAppendHtml(page, pageBlock.subtitle)
        )
    }
}


object HtmlPageBlockSubheader : HtmlMapper<TdApi.PageBlockSubheader>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockSubheader): DomContent {
        return TagCreator.h4(
            findAppendHtml(page, pageBlock.subheader)
        )
    }
}

object HtmlPageBlockPreformatted : HtmlMapper<TdApi.PageBlockPreformatted>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockPreformatted): DomContent {
        return TagCreator.pre(
            findAppendHtml(page, pageBlock.text)
        ).attr("data-language", pageBlock.language)
    }
}

object HtmlPageBlockBlockQuote : HtmlMapper<TdApi.PageBlockBlockQuote>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockBlockQuote): DomContent {
        return TagCreator.blockquote(
            TagCreator.cite(findAppendHtml(page, pageBlock.credit)),
            findAppendHtml(page, pageBlock.text)
        )
    }
}

object HtmlPageBlockEmbedded : HtmlMapper<TdApi.PageBlockEmbedded>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockEmbedded): DomContent {
        if (pageBlock.html.isNotEmpty() && pageBlock.url.isEmpty()) {
            return TagCreator.div(pageBlock.html)
        }

        //TODO: impl posterPhoto
        return TagCreator.figure(
            TagCreator.iframe()
                .attr(Attr.WIDTH, pageBlock.width)
                .attr(Attr.HEIGHT, pageBlock.height)
                .condAttr(pageBlock.allowScrolling, "scrolling", null)
                .withSrc(pageBlock.url),
            findAppendHtml(page, pageBlock.caption)
        )
    }
}

abstract class DownloadableResource<T : TdApi.Object> : HtmlMapper<T>() {
    abstract fun getFile(pageBlock: T): TdApi.File

    abstract fun createComponent(page: InstantViewPage, pageBlock: T, src: String): DomContent

    abstract fun findSrc(localFile: TdApi.LocalFile): String

    override fun appendHtml(page: InstantViewPage, pageBlock: T): DomContent {
        val file = getFile(pageBlock)
        return if (file.local.path.isNotEmpty()) {
            createComponent(page, pageBlock, findSrc(file.local))
        } else {
            val resId = page.registerAsyncDownload(file, pageBlock,this)
            TagCreator.span("Resource loading..").withClass("wsPull").attr("resId", resId)
        }
    }


}

fun TdApi.LocalFile.pathFromLastSlash(): String {
    return this.path.run {
        this.substring(this.lastIndexOf("/") + 1)
    }
}

object HtmlPageBlockCover : HtmlMapper<TdApi.PageBlockCover>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockCover): DomContent {
        return TagCreator.div(
            TagCreator.attrs(".cover"),
            findAppendHtml(page, pageBlock.cover)
        )
    }
}

object HtmlPageBlockCaption : HtmlMapper<TdApi.PageBlockCaption>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockCaption): DomContent {
        return TagCreator.figcaption(
            TagCreator.attrs(".caption"),
            findAppendHtml(page, pageBlock.text),
            findAppendHtml(page, pageBlock.credit)
        )
    }
}

object HtmlPageBlockAudio : DownloadableResource<TdApi.PageBlockAudio>() {
    override fun getFile(pageBlock: TdApi.PageBlockAudio): TdApi.File {
        return pageBlock.audio.audio
    }

    override fun createComponent(page: InstantViewPage, pageBlock: TdApi.PageBlockAudio, src: String): DomContent {
        return TagCreator.figure(
            TagCreator.audio().attr(Attr.CONTROLS).withSrc(src),
            findAppendHtml(page, pageBlock.caption)
        )
    }

    override fun findSrc(localFile: TdApi.LocalFile): String {
        return RES_AUDIO.buildSrc(localFile)
    }
}


object HtmlPageBlockVideo : DownloadableResource<TdApi.PageBlockVideo>() {
    override fun getFile(pageBlock: TdApi.PageBlockVideo): TdApi.File {
        return pageBlock.video.video
    }

    override fun createComponent(page: InstantViewPage, pageBlock: TdApi.PageBlockVideo, src: String): DomContent {
        return TagCreator.figure(
            TagCreator.video().attr(Attr.CONTROLS)
                .condAttr(pageBlock.isLooped, Attr.LOOP, null)
                .condAttr(pageBlock.needAutoplay, Attr.AUTOPLAY, null)
                .withSrc(src),
            findAppendHtml(page, pageBlock.caption)
        )
    }

    override fun findSrc(localFile: TdApi.LocalFile): String {
        return RES_VIDEO.buildSrc(localFile)
    }

}

object HtmlPageBlockAnimation : DownloadableResource<TdApi.PageBlockAnimation>() {
    override fun getFile(pageBlock: TdApi.PageBlockAnimation): TdApi.File {
        return pageBlock.animation.animation
    }

    override fun createComponent(page: InstantViewPage, pageBlock: TdApi.PageBlockAnimation, src: String): DomContent {
        return TagCreator.figure(
            TagCreator.video()
                .condAttr(pageBlock.needAutoplay, Attr.AUTOPLAY, null)
                .attr(Attr.LOOP)
                .attr("muted")
                .attr("playsinline").withSrc(src),
            findAppendHtml(page, pageBlock.caption)
        )
    }

    override fun findSrc(localFile: TdApi.LocalFile): String {
        return RES_ANIMATION.buildSrc(localFile)
    }
}

object HtmlPageBlockPhoto : DownloadableResource<TdApi.PageBlockPhoto>() {
    override fun getFile(pageBlock: TdApi.PageBlockPhoto): TdApi.File {
        return pageBlock.photo.sizes.last().photo
    }

    override fun createComponent(page: InstantViewPage, pageBlock: TdApi.PageBlockPhoto, src: String): DomContent {
        return TagCreator.figure(
            TagCreator.img().withSrc(src),
            findAppendHtml(page, pageBlock.caption)
        )
    }

    override fun findSrc(localFile: TdApi.LocalFile): String {
        return RES_PHOTO.buildSrc(localFile)
    }
}

object HtmlRichTextUrl : HtmlMapper<TdApi.RichTextUrl>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextUrl): DomContent {
        return TagCreator.a(
            findAppendHtml(page, pageBlock.text)
        ).withHref(pageBlock.url)
    }
}

object HtmlRichTexts : HtmlMapper<TdApi.RichTexts>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTexts): DomContent {
        return TagCreator.div(
            TagCreator.each(pageBlock.texts.toList()) { rt ->
                findAppendHtml(page, rt)
            }
        )
    }
}

object HtmlPageBlockParagraph : HtmlMapper<TdApi.PageBlockParagraph>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockParagraph): DomContent {
        return TagCreator.p(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlRichTextItalic : HtmlMapper<TdApi.RichTextItalic>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextItalic): DomContent {
        return TagCreator.i(findAppendHtml(page, pageBlock.text))
    }
}


object HtmlRichTextPlain : HtmlMapper<TdApi.RichTextPlain>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextPlain): DomContent {
        return TagCreator.span(pageBlock.text)
    }
}

object HtmlRichTextBold : HtmlMapper<TdApi.RichTextBold>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.RichTextBold): DomContent {
        return TagCreator.strong(findAppendHtml(page, pageBlock.text))
    }
}

object HtmlPageBlockHeader : HtmlMapper<TdApi.PageBlockHeader>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockHeader): DomContent {
        return TagCreator.h2(findAppendHtml(page, pageBlock.header))
    }
}

object HtmlPageBlockTitle : HtmlMapper<TdApi.PageBlockTitle>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockTitle): DomContent {
        return TagCreator.h1(findAppendHtml(page, pageBlock.title))
    }
}

object HtmlPageBlockAuthorDate : HtmlMapper<TdApi.PageBlockAuthorDate>() {
    override fun appendHtml(page: InstantViewPage, pageBlock: TdApi.PageBlockAuthorDate): DomContent {
        //TODO: implement user timezone
        return TagCreator.p(
            TagCreator.attrs(".author"),
            TagCreator.span((pageBlock.publishDate * 1000L).toString()).withClass("timestamp"),
            TagCreator.span(" by "),
            findAppendHtml(page, pageBlock.author)
        )
    }
}


abstract class HtmlMapper<T : TdApi.Object> {
    abstract fun appendHtml(page: InstantViewPage, pageBlock: T): DomContent
}