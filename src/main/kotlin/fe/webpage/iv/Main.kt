package fe.webpage.iv

import com.google.gson.Gson
import com.google.gson.JsonObject
import fe.gson.extensions.fileToInstance
import fe.webpage.iv.config.Config
import fe.webpage.iv.html.findAppendHtml
import fe.webpage.iv.html.pathFromLastSlash
import fe.webpage.iv.route.InstantViewRoute
import fe.webpage.iv.route.ResourceRoute
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.http.staticfiles.Location
import j2html.TagCreator.*
import j2html.attributes.Attr
import org.drinkless.tdlib.Connector
import org.drinkless.tdlib.TdApi
import java.io.*
import java.util.*


lateinit var CONFIG: Config


const val RES_PATH = "/res"

val RES_PHOTO = ResourceDir("$RES_PATH/photo", "photos")
val RES_ANIMATION = ResourceDir("$RES_PATH/animation", "animations")
val RES_VIDEO = ResourceDir("$RES_PATH/video", "videos")
val RES_AUDIO = ResourceDir("$RES_PATH/voice", "voice")

data class ResourceDir(val route: String, val dir: String) {
    fun registerRoute(app: Javalin, config: Config) {
        val dir = File(config.parameters.databaseDirectory, dir)
        app.get("$route/:id") { ctx ->
            ctx.result(FileInputStream(File(dir, ctx.pathParam("id"))))
        }
    }

    fun buildSrc(localFile: TdApi.LocalFile) = "$route/${localFile.pathFromLastSlash()}"
}

////does audio use voice folder?
val RES_DIRS = listOf(
    RES_PHOTO, RES_ANIMATION, RES_VIDEO, RES_AUDIO
)

const val RESOURCE_DIR = "src/main/resources"
val PAGE_CACHE_DIR = File("page_cache_dir").also {
    it.mkdirs()
}

fun main(args: Array<String>) {
    if (args.size != 1) error("Please provide path to config!")

    Gson().fileToInstance<Config>(args[0]).also { CONFIG = it }.run {
        Connector.createInstance(this.tdlibPath, this.parameters)
    }.startup()

    val app = Javalin.create { config ->
        config.addSinglePageRoot("/", "$RESOURCE_DIR/static/index.html", Location.EXTERNAL)
        config.addStaticFiles("/static", "$RESOURCE_DIR/static", Location.EXTERNAL)
    }.start(7000)

    RES_DIRS.forEach { resourceDir ->
        resourceDir.registerRoute(app, CONFIG)
    }

    app.routes {
        path("/iv") {
            post("/api", InstantViewRoute::api)
            post("/", InstantViewRoute::formPage)
        }
    }

    app.ws("/ws/res") { ws ->
        ws.onMessage(ResourceRoute)
    }
}

fun JsonObject.getAsStringIfHas(key: String): String? {
    return if (this.has(key)) {
        this.getAsJsonPrimitive(key).asString
    } else null
}

//fun generateHtml(url: String, webPage: TdApi.WebPageInstantView): String {
//    return html(
//        head(
//            title("IV - $url"),
//            meta().withCharset("utf-8"),
//            meta().attr("property", "og:url").attr("content", url),
//            link().withRel("stylesheet").withHref("/static/css/normalize.css"),
//            link().withRel("stylesheet").withHref("/static/css/default.css"),
//            script().withSrc("/static/js/resource-pull.js")
//        ).attr(Attr.DIR, if (webPage.isRtl) "rtl" else "ltr"),
//        body(
//            a("← Original Page").withHref(url).attr(Attr.TARGET, "_blank"),
//            each(webPage.pageBlocks.toList()) { block ->
//                findAppendHtml(block)
//            }
//        )
//    ).render()
////        .also { page ->
////        if (CONFIG.cacheBuiltPages) {
////            File(PAGE_CACHE_DIR, Base64.getEncoder().encodeToString(url.toByteArray()) + ".html").writeText(page)
////        }
////    }
//}
